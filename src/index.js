import videojs from "video.js";
import io from "socket.io-client/dist/socket.io.slim.js";
import srt2webvtt from './webvtt.js';
//import(/* webpackPreload: true */ "video.js/dist/video.min.js");
//import videojs from './video.min.js'
var inputStatusDisable = true
setInterval(()=>document.getElementById("last-played").textContent = localStorage.getItem("last-watched"),5000)

//video.js and video.js-City theme CSS
require("!style-loader!css-loader!video.js/dist/video-js.min.css");
require("!style-loader!css-loader!./css/index.css");
var volume = document.getElementById('volume')
var inputState = document.getElementById('input-state');
inputState.onchange = (e) =>{
	if(e.target.checked){
		document.querySelector('.blocked').style.pointerEvents = "none";

	}else{
		document.querySelector('.blocked').style.pointerEvents = "auto";
	}
}
console.log(volume)

//Vars
const socket_URI = "https://guarded-dawn-23643.herokuapp.com/";
const socket = io(socket_URI);
const videoElement = document.querySelector("video");
var player = videojs("#my-video");
volume.oninput = (e) =>{
	console.log(player.volume(volume.value))
}
function getHost(){
	if(window.location.href.split('?h=')[1] == "ORZHKZI="){
		return true
	}else{
		return false
	}
}
if(getHost()){
	document.getElementById("host").textContent = "Host -> true"
}else{
	document.getElementById("host").textContent = "Host -> false"
}
socket.on("play", function (time) {
	
	videoElement.currentTime = parseFloat(time.time);
	videoElement.play();
});
socket.on("pause", function (time) {
	videoElement.currentTime = parseFloat(time.time);
	videoElement.pause();
});
socket.on("seeked", function (time) {
	videoElement.currentTime = time.time;
});
document
	.getElementById("fullscreen")
	.addEventListener("click", () =>
		player.requestFullscreen()
	);
var vdFile = document.getElementById("load");
vdFile.onchange = (e) => {
	var files = e.target.files[0];
	localStorage.setItem("last-watched", files.name)
	var url = URL.createObjectURL(files);
	player.src({
		type: "video/mp4",
		src: url,
	});
};
var subFile = document.getElementById("loadsub");
subFile.onchange = (e) => {
	var files = e.target.files[0];
	var url = URL.createObjectURL(files);
	var reader = new FileReader();
	reader.readAsText(files);
	reader.onload = () => {
		const converted = srt2webvtt(reader.result);
		const myBlob = new Blob([converted], { type: "text/plain" });
		videojs("#my-video").addRemoteTextTrack({
			label: "Sub",
			src: URL.createObjectURL(myBlob),
		});
	};
};
setTimeout(function () {
	InitialiazeEvents();
	document.getElementById("status").textContent = "Connection Status -> "+socket.connected
}, 5000);
function InitialiazeEvents() {
	var isAdmin = getHost();
	if (socket.connected || isAdmin) {
		videoElement.play();
		videoElement.addEventListener("play", function () {
			if (isAdmin) {
				socket.emit("play", videoElement.currentTime);
			}
		});
		videoElement.addEventListener("pause", function () {
			if (isAdmin) {
				socket.emit("pause", videoElement.currentTime);
			}
		});
	} else {

	}
}
